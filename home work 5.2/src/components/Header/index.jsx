import styles from './Header.module.scss';
import CartIcon from '../svg/CartIcon';
import StarIcon from '../svg/StarIcon'

const Header = ({favorite = [], cartItems}) => {
  return (
    <header className={styles.header}>
      <span className={styles.logo}>Rozetka Books</span>

      <nav>
        <ul>
          <li>
            <a href="/#">Home</a>
          </li>

          <li>
            <div className={styles.containers}>
              <a href='/#' className={styles.cartContainer}>
                <CartIcon />
                <span>{cartItems.length}</span>
              </a>
              <a href='/#' className={styles.cartContainer}>
                <StarIcon />
                <span>{favorite.length}</span>
              </a>
            </div>
          </li>

          <li>
            <a href="/#">
              <span>Log In</span>
            </a>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header;
