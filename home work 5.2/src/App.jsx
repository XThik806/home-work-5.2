import { useState, useEffect } from 'react'
import ProductCard from './components/ProductCard'
import axios from 'axios'
import ProductContainer from './components/ProductContainer'
import Header from './components/Header'
import { array } from 'prop-types'
import ModalBody from './components/Modal/ModalBody'

function App() {
  const [products, setProducts] = useState([]);
  const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem('favorite')) || []);
  const [cartItems, setCartItems] = useState(JSON.parse(localStorage.getItem('cartItems')) || []);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [curentProduct, setCurrentProduct] = useState({})

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const { data } = await axios.get('./data.json');
        setProducts(data);

        const getFavorite = localStorage.getItem(favorite)
        if (getFavorite && array.isArray(getFavorite)) {
          setFavorite(getFavorite);
        }
      } catch (err) {
        console.log(err);
      }
    }

    fetchProducts()
  }, [])

  const isFavorite = (product, setIsClicked) => {
    const indexItem = favorite.findIndex(
      (item) => item.id === product.id
    );

    if (indexItem !== -1) {
      setIsClicked(true)
    }
  }

  const addToFavorite = (product, setIsClicked) => {
    const indexItem = favorite.findIndex(
      (item) => item.id === product.id
    );

    if (indexItem === -1) {
      const newItem = {
        id: product.id,
        name: product.name,
        genre: product.genre,
        cover: product.cover,
      };
      const mergeFavorite = [...favorite, newItem];

      setFavorite(mergeFavorite);
      localStorage.setItem("favorite", JSON.stringify(mergeFavorite));
      setIsClicked(true);
      console.log(mergeFavorite);
    }

    else {
      const updatedFavorite = favorite.filter(
        (item) => item.id !== product.id
      );
      setFavorite(updatedFavorite);
      localStorage.setItem(
        "favorite",
        JSON.stringify(updatedFavorite)
      );
      setIsClicked(false);
      console.log(updatedFavorite);
    }
  }

  const addToCart = (curentProduct) => {
    const indexItem = cartItems.findIndex(
      (item) => item.id === curentProduct.id
    );

    if (indexItem === -1) {
      const newItem = {
        id: curentProduct.id,
        name: curentProduct.name,
        genre: curentProduct.genre,
        cover: curentProduct.cover,
      };
      const mergeCart = [...cartItems, newItem];

      setCartItems(mergeCart);
      localStorage.setItem("cartItems", JSON.stringify(mergeCart));
      console.log(mergeCart);
    }
  }

  const modalActivate = () => {
    setIsModalOpen(!isModalOpen)
    console.log(isModalOpen);
  };

  // let curentProduct;

  return (
    <>
      {/* <button onClick={() => {
        localStorage.clear()
      }}>clear local storage</button> */}
      <Header favorite={favorite} cartItems={cartItems}></Header>

      <ProductContainer products={products} addToFavorite={addToFavorite} isFavorite={isFavorite} modalActivate={modalActivate} setCurrentProduct={setCurrentProduct}/>

      {isModalOpen && <ModalBody modalActivate={modalActivate} curentProduct={curentProduct} addToCart={addToCart}></ModalBody>}
    </>
  )
}

export default App
